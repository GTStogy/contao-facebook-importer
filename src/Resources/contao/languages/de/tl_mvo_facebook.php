<?php

declare(strict_types=1);

/*
 * Contao Facebook Import Bundle for Contao Open Source CMS
 *
 * @copyright  Copyright (c) 2017-2018, Moritz Vondano
 * @license    MIT
 * @link       https://github.com/m-vo/contao-facebook-import
 *
 * @author     Moritz Vondano
 */

$GLOBALS['TL_LANG']['tl_mvo_facebook']['new'] = ['Neuen Facebook Knoten hinzufügen'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['edit'][1] = 'Facebook Knoten bearbeiten';
$GLOBALS['TL_LANG']['tl_mvo_facebook']['events'][1] = 'Importierte Veranstaltungen anzeigen';
$GLOBALS['TL_LANG']['tl_mvo_facebook']['posts'][1] = 'Importierte Posts anzeigen';
$GLOBALS['TL_LANG']['tl_mvo_facebook']['toggle'][1] = 'Auto-Import aktivieren/deaktivieren';
$GLOBALS['TL_LANG']['tl_mvo_facebook']['delete'][1] = 'Facebook Knoten löschen';


$GLOBALS['TL_LANG']['tl_mvo_facebook']['basic_legend'] = 'Allgemeines';
$GLOBALS['TL_LANG']['tl_mvo_facebook']['description'] = ['Bezeichner'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['fbPageName'] = ['Name oder ID der Facebook Seite'];

$GLOBALS['TL_LANG']['tl_mvo_facebook']['api_legend'] = 'Facebook API';
$GLOBALS['TL_LANG']['tl_mvo_facebook']['fbAppId'] = ['Facebook App ID'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['fbAppSecret'] = ['Facebook App Secret'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['fbAccessToken'] = ['Nie ablaufender Facebook Access Token', 'Das System wird automatisch versuchen einen nie ablaufenden Token aus dem angegebenen Token zu generieren.'];

$GLOBALS['TL_LANG']['tl_mvo_facebook']['import_legend'] = 'Import';
$GLOBALS['TL_LANG']['tl_mvo_facebook']['importEnabled'] = ['Auto-Import aktivieren'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['minimumCacheTime'] = ['Minimales Cache-Alter', 'minimale Zeit in Sekunden, bevor Elemente neu importiert werden'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['numberOfPosts'] = ['Post-Anzahl','maximale Anzahl an importierten Posts'];


$GLOBALS['TL_LANG']['tl_mvo_facebook']['news_legend'] = 'Post Einstellungen';
$GLOBALS['TL_LANG']['tl_mvo_facebook']['event_legend'] = 'Event Einstellungen';
$GLOBALS['TL_LANG']['tl_mvo_facebook']['createNativeNews'] = ['Zusätzlich native Contao News erzeugen','Fügt beim Import Contao News hinzu'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['createNativeEvents'] = ['Zusätzlich native Contao Events erzeugen','Fügt beim Import Contao Events hinzu, die Facebook Veranstaltungen referenzieren'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['calendarId'] = ['Kalender', 'Kalender wählen, in den die Veranstaltungen eingefügt werden sollen.'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['newsArchiveId'] = ['Newsarchive', 'Newsarchive wählen, in den die Posts eingefügt werden sollen.'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['calendarEventAuthor'] = ['Autor', 'Autor wählen, der den erstellten Events zugewiesen werden soll.'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['newsAuthor'] = ['Autor', 'Autor wählen, der den erstellten Posts zugewiesen werden soll.'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['newsDefaultImage'] = ['Bild', 'Bild auswählen'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['setNewsDefaultImage'] = ['Post Default Bild setzen', 'Default Bild wählen, falls das Event kein Bild hat.'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['calendarEventDefaultImage'] = ['Bild', 'Bild auswählen'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['setCalendarEventDefaultImage'] = ['Event Default Bild setzen', 'Default Bild wählen, falls das Event kein Bild hat.'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['newsTeaserLength'] = ['Max. Teaser Wörter', 'Legt die maximal Anzahl an Wörter für einen News Teaser fest'];
$GLOBALS['TL_LANG']['tl_mvo_facebook']['calendarEventTeaserLength'] = ['Max. Teaser Wörter', 'Legt die maximal Anzahl an Wörter für einen Event Teaser fest'];

$GLOBALS['TL_LANG']['tl_mvo_facebook']['media_legend'] = 'Medien';
$GLOBALS['TL_LANG']['tl_mvo_facebook']['uploadDirectory'] = ['Upload-Ordner', 'Ort, an dem die gescrapten Bilddaten ablegt werden'];
