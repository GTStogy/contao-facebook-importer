<?php

declare(strict_types=1);

/*
 * Contao Facebook Import Bundle for Contao Open Source CMS
 *
 * @copyright  Copyright (c) 2017-2018, Moritz Vondano
 * @license    MIT
 * @link       https://github.com/m-vo/contao-facebook-import
 *
 * @author     Moritz Vondano
 */

$GLOBALS['TL_LANG']['tl_content']['mvo_facebook_options_legend'] =
    'Optionen für Facebook-Content';
$GLOBALS['TL_LANG']['tl_content']['mvo_facebook_node']           =
    ['Facebook Knoten', 'Knoten auswählen, von dem Daten angezeigt werden sollen'];
$GLOBALS['TL_LANG']['tl_content']['mvo_facebook_numberOfPosts'] =
    ['Anzahl an Posts', '0 eingeben, um alle Einträge anzuzeigen'];
