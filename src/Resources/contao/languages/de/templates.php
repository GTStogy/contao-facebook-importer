<?php

declare(strict_types=1);

/*
 * Contao Facebook Import Bundle for Contao Open Source CMS
 *
 * @copyright  Copyright (c) 2017-2018, Moritz Vondano
 * @license    MIT
 * @link       https://github.com/m-vo/contao-facebook-import
 *
 * @author     Moritz Vondano
 */

$GLOBALS['TL_LANG']['MSC']['mvo_facebook_emptyPostList'] = 'Derzeit sind keine Posts verfügbar.';
$GLOBALS['TL_LANG']['MSC']['mvo_facebook_emptyEventList'] = 'Derzeit sind keine Veranstaltungen verfügbar.';
