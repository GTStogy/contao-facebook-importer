<?php

declare(strict_types=1);

/*
 * Contao Facebook Import Bundle for Contao Open Source CMS
 *
 * @copyright  Copyright (c) 2017-2018, Moritz Vondano
 * @license    MIT
 * @link       https://github.com/m-vo/contao-facebook-import
 *
 * @author     Moritz Vondano
 */

$GLOBALS['TL_LANG']['tl_mvo_facebook_event']['import'] = 'Force importing events from Facebook';
$GLOBALS['TL_LANG']['tl_mvo_facebook_event']['show'][1] = 'Show the event details';
$GLOBALS['TL_LANG']['tl_mvo_facebook_event']['delete'][1] = 'Delete event';
$GLOBALS['TL_LANG']['tl_mvo_facebook_event']['toggle'][1] = 'Show/hide event in frontend';