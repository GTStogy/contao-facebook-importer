<?php

declare(strict_types=1);

/*
 * Contao Facebook Import Bundle for Contao Open Source CMS
 *
 * @copyright  Copyright (c) 2017-2018, Moritz Vondano
 * @license    MIT
 * @link       https://github.com/m-vo/contao-facebook-import
 *
 * @author     Moritz Vondano
 */

$GLOBALS['TL_LANG']['tl_content']['mvo_facebook_options_legend'] =
    'Facebook Content Options';
$GLOBALS['TL_LANG']['tl_content']['mvo_facebook_node']           =
    ['Facebook Node', 'Select node to show entries from'];
$GLOBALS['TL_LANG']['tl_content']['mvo_facebook_numberOfPosts']  =
    ['Number of Posts', 'Enter 0 to show all available posts'];
