<?php

declare(strict_types=1);

/*
 * Contao Facebook Import Bundle for Contao Open Source CMS
 *
 * @copyright  Copyright (c) 2017-2018, Moritz Vondano
 * @license    MIT
 * @link       https://github.com/m-vo/contao-facebook-import
 *
 * @author     Moritz Vondano
 */

namespace Mvo\ContaoFacebookImport\EventListener;

use Contao\ContentModel;
use Contao\Dbafs;
use Contao\Files;
use Contao\FilesModel;
use Contao\NewsArchiveModel;
use Contao\NewsModel;
use Contao\StringUtil;
use Contao\Model\Collection;
use Facebook\GraphNodes\GraphNode;
use Mvo\ContaoFacebookImport\Facebook\OpenGraphParser;
use Mvo\ContaoFacebookImport\Model\FacebookModel;
use Mvo\ContaoFacebookImport\Model\FacebookPostModel;
use Mvo\ContaoFacebookImport\String\Tools;

class ImportFacebookPostsListener extends ImportFacebookDataListener
{
    /**
     * @param integer $pid
     *
     * @return integer
     */
    protected function getLastTimeStamp(int $pid): int
    {
        return FacebookPostModel::getLastTimestamp($pid);
    }

    /**
     * Entry point: Import/update facebook events.
     *
     * @param FacebookModel   $node
     * @param OpenGraphParser $parser
     *
     * @throws \InvalidArgumentException
     */
    protected function import(FacebookModel $node, OpenGraphParser $parser): void
    {
        // find existing posts
        $objPosts       = FacebookPostModel::findByPid($node->id);
        $postDictionary = [];
        if (null !== $objPosts) {
            foreach ($objPosts as $objPost) {
                /** @var FacebookPostModel $objPost */
                $postDictionary[$objPost->postId] = $objPost;
            }
        }
        // query facebook for current posts
        $graphEdge = $parser->queryEdge(
            'posts',
            [
                'id',
                'created_time',
                'attachments{media_type,target{id}}',
                'message',
                'full_picture',
                'updated_time'
            ],
            ['limit' => $node->numberOfPosts]
        );
        if (null === $graphEdge) {
            return;
        }

        // merge the data
        $uploadDirectory = FilesModel::findById($node->uploadDirectory);
        if (!$uploadDirectory || !$uploadDirectory->path) {
            throw new \InvalidArgumentException('No or invalid upload path.');
        }

        /** @var GraphNode $graphNode */
        foreach ($graphEdge as $graphNode) {
            $fbId = $graphNode->getField('id', null);
            if ($fbId === null) {
                continue;
            }

            // skip if message is empty or type is 'link' or 'event' and the message only contains an URL
            $message = $graphNode->getField('message', '');
            if ('' === $message) {
                continue;
            }

            $type = $this->getType($graphNode);
            if ($type === 'event' || ($type === 'link' && preg_match('~^\s*https://\S*\s*?$~', $message))) {
                continue;
            }

            if (\array_key_exists($fbId, $postDictionary)) {
                // update existing item
                if ($this->updateRequired($graphNode, $postDictionary[$fbId])) {
                    $this->updatePost($parser, $postDictionary[$fbId], $graphNode, $uploadDirectory->path, $node);
                }
                unset($postDictionary[$fbId]);

            } else {
                // create new item
                $post = new FacebookPostModel();

                $post->pid    = $node->id;
                $post->postId = $fbId;
                $this->updatePost($parser, $post, $graphNode, $uploadDirectory->path, $node);

                // create referencing event
                if ($node->createNativeNews) {
                    $this->createReferencingPost($node, $post);
                }
            }
        }

        // remove orphans
        /** @var FacebookPostModel $post */
        foreach ($postDictionary as $post) {
            // todo: generalize with dca's ondelete_callback
            if ($post->image && $file = FilesModel::findByUuid($post->image)) {
                /** @var Collection $objPosts */
                $objPosts = FacebookPostModel::findBy('image', $post->image);
                if (1 === $objPosts->count()) {
                    Files::getInstance()->delete($file->path);
                    Dbafs::deleteResource($file->path);
                }
            }
            $post->delete();
        }
    }

    /**
     * @param GraphNode         $graphNode
     * @param FacebookPostModel $post
     *
     * @return bool
     */
    private function getType(GraphNode $graphNode): ?string
    {
        foreach ($graphNode->getField('attachments', []) as $attachment) {
            return $attachment['media_type'];
        }
        return null;
    }

    /**
     * @param GraphNode         $graphNode
     * @param FacebookPostModel $post
     *
     * @return bool
     */
    private function updateRequired(GraphNode $graphNode, FacebookPostModel $post): bool
    {
        return $this->getTime($graphNode, 'updated_time') !== $post->lastChanged;
    }

    /**
     * @param OpenGraphParser   $parser
     * @param FacebookPostModel $post
     * @param GraphNode         $graphNode
     * @param string            $uploadPath
     */
    private function updatePost(
        OpenGraphParser $parser,
        FacebookPostModel $post,
        GraphNode $graphNode,
        string $uploadPath,
        FacebookModel $node
    ): void {
        $post->tstamp      = \time();
        $post->postTime    = $this->getTime($graphNode, 'created_time');
        $post->message     = $graphNode->getField('message', '');
        $post->lastChanged = $this->getTime($graphNode, 'updated_time');

        $post->image       = $this->getImage($parser, $graphNode, $uploadPath);
        if (!$post->image && $node->setNewsDefaultImage && $node->newsDefaultImage) {
          $image             = FilesModel::findById($node->newsDefaultImage);
          $post->image      = $image ? $image->uuid : null;
        }

        /** @noinspection ExceptionsAnnotatingAndHandlingInspection */
        $post->save();
    }

    /**
     * @param GraphNode $graphNode
     * @param string    $field
     *
     * @return int
     */
    private function getTime(GraphNode $graphNode, string $field): int
    {
        /** @var \DateTime $date */
        $date = $graphNode->getField($field, null);
        return ($date !== null) ? $date->getTimestamp() : 0;
    }

    /**
     * @param OpenGraphParser $parser
     * @param GraphNode       $graphNode
     * @param string          $uploadPath
     *
     * @return null|string
     */
    private function getImage(OpenGraphParser $parser, GraphNode $graphNode, string $uploadPath): ?string
    {
        $picture = $graphNode->getField('full_picture', null);
        if (null === $picture) {
            return null;
        }

        // scrape/retrieve image
        $fileModel = null;
        $attachments = $graphNode->getField('attachments', null);
        $attachment = $attachments === null ? null : $attachments[0];
        $objectId = null;

        if ($attachment !== null && $attachment['target'] !== null) {
            $objectId = $attachment['target']['id'];
            $type = $attachment['media_type'];
            $fileModel = $this->imageScraper->scrapeObject(
                $parser,
                $type,
                $objectId,
                $uploadPath
            );
        }

         if (null === $fileModel) {
            $fileModel = $this->imageScraper->scrapeFile(
                'p_' . $graphNode->getField('id'),
                $picture,
                $uploadPath
            );
        }

        // update meta data
        if (null !== $fileModel) {
            $fileModel->name = $objectId;

            /** @noinspection ExceptionsAnnotatingAndHandlingInspection */
            $fileModel->save();
        }

        return (null !== $fileModel) ? $fileModel->uuid : null;
    }

    /**
     * Create a new node in the selected calendar.
     *
     * @param FacebookModel      $node
     * @param FacebookPostModel $event
     */
    private function createReferencingPost(FacebookModel $node, FacebookPostModel $post): void
    {
        if (0 === NewsArchiveModel::countById($node->newsArchiveId)) {
            return;
        }

        $headline = Tools::formatText($post->message, 5);
        if (false !== $posBreak = strpos($headline, '<br>')) {
            $headline = substr($headline, 0, $posBreak);
        }

        $alias = "news-$post->postId";
        if (NewsModel::countByAlias($alias) > 0) {
          return;
        }
        $news = new NewsModel();
        $news->pid = $node->newsArchiveId;
        $news->alias = $alias;
        $news->tstamp = $post->tstamp;
        $news->headline = $headline;
        $news->teaser = "<p>" . Tools::formatText($post->message, intval($node->newsTeaserLength)) . "</p>";
        $news->date = $post->postTime;
        $news->time = $post->postTime;
        $news->published = $post->visible;
        $news->source = 'default';
        if($post->image) {
            $news->addImage = 1;
            $news->singleSRC = $post->image;
        }

        /** @noinspection ExceptionsAnnotatingAndHandlingInspection */
        $news->save();

        $content = new ContentModel();
        $content->pid = $news->id;
        $content->ptable = 'tl_news';
        $content->tstamp = $post->tstamp;
        $content->type = 'text';
        $content->text = Tools::formatText($post->message);
        if($post->image) {
            $content->addImage = 1;
            $content->singleSRC = $post->image;
        }

        /** @noinspection ExceptionsAnnotatingAndHandlingInspection */
        $content->save();
    }
}
