### v2.2.0
 - allow automatic creation of native Contao events on import
 - insert tag for event data {{fb_event::@eventId::@parameter}}

### v2.1.1
 - fixed and optimized image pruning

### v2.1.0
 - added automatic live long token generation via the backend

### v2.0.0
 - lots of small fixes and optimizations
 - code reorganization

 *Migrating from v1:*
 If you did not interact with the code programmatically, there is
 nothing that you need to do. Just adapt your composer requirement of
 the bundle to `^2.0`.